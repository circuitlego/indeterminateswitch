//
//  CLSwitchController.swift
//  CLSwitch
//
//  Created by Gabriel Fernandez on 11/14/14.
//  Copyright (c) 2014 Gabriel Fernandez. All rights reserved.
//

import UIKit

class CLSwitchController {
    var mainSwitch: CLSwitch?
    var switches: [UISwitch] = []
    
    func registerSwitch(control: UISwitch) {
        switches.append(control)
        control.addTarget(self, action: "toggleSwitch:", forControlEvents: .ValueChanged)
    }
    
    func toggleSwitch(sender: AnyObject) {
        var someOn: Bool = true
        var someOff: Bool = true
        
        for control in switches {
            someOn = control.on && someOn
            someOff = !control.on && someOff
        }
        
        if someOn && someOff{
            mainSwitch?.setState(.Indeterminate, animated: true);
        } else if someOn && !someOff{
            mainSwitch?.setState(.On, animated: true)
        } else {
            mainSwitch?.setState(.Off, animated: true)
        }
    }
}