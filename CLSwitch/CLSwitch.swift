//
//  SevenSwitch.swift
//
//  Created by Benjamin Vogelzang on 6/20/14.
//  Copyright (c) 2014 Ben Vogelzang. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit
import QuartzCore

enum SwitchState {
    case Indeterminate
    case On
    case Off
}

@objc @IBDesignable class CLSwitch: UIControl {
    
    // public
    
    /*
    *   Set (without animation) whether the switch is on or off
    */
    var switchState: SwitchState {
        get {
            return switchValue
        }
        set {
            switchValue = newValue
            self.setState(newValue, animated: false)
        }
    }
    
    /*
    *	Sets the background color that shows when the switch off and actively being touched.
    *   Defaults to light gray.
    */
    var activeColor: UIColor = UIColor(red: 0.89, green: 0.89, blue: 0.89, alpha: 1)
    
    /*
    *	Sets the background color when the switch is off.
    *   Defaults to clear color.
    */
    var inactiveColor: UIColor = UIColor.clearColor()
    
    /*
    *   Sets the background color that shows when the switch is on.
    *   Defaults to green.
    */
    var onTintColor: UIColor = UIColor(red: 0.3, green: 0.85, blue: 0.39, alpha: 1)
    
    //var offTintColor: UIColor
    
    /*
    *   Sets the border color that shows when the switch is off. Defaults to light gray.
    */
    var borderColor: UIColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
    
    /*
    *	Sets the knob color. Defaults to white.
    */
    var thumbTintColor: UIColor = UIColor.whiteColor()
    
    /*
    *	Sets the knob color that shows when the switch is on. Defaults to white.
    */
    var onThumbTintColor: UIColor = UIColor.whiteColor()
    
    /*
    *	Sets the shadow color of the knob. Defaults to gray.
    */
    var shadowColor: UIColor = UIColor.grayColor()
    
    /*
    *	Sets whether or not the switch edges are rounded.
    *   Set to NO to get a stylish square switch.
    *   Defaults to YES.
    */
    var isRounded: Bool = true 
    
    
    // private
    var backgroundView: UIView!
    var thumbView: UIView!
    var currentVisualValue: SwitchState = .Off
    var startTrackingValue: SwitchState = .Off
    var didChangeWhileTracking: Bool = false
    var isAnimating: Bool = false
    var userDidSpecifyOnThumbTintColor: Bool = false
    var switchValue: SwitchState = .Off
    
    /*
    *   Initialization
    */
    override init() {
        super.init(frame: CGRectMake(0, 0, 50, 30))
        
        self.setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    override init(frame: CGRect) {
        let initialFrame = CGRectIsEmpty(frame) ? CGRectMake(0, 0, 50, 30) : frame
        super.init(frame: initialFrame)
        
        self.setup()
    }
    
    /*
    *   Setup the individual elements of the switch and set default values
    */
    func setup() {
        
        // background
        self.backgroundView = UIView(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
        backgroundView.backgroundColor = UIColor.clearColor()
        backgroundView.layer.cornerRadius = self.frame.size.height * 0.5
        backgroundView.layer.borderColor = self.borderColor.CGColor
        backgroundView.layer.borderWidth = 1.0
        backgroundView.userInteractionEnabled = false
        backgroundView.clipsToBounds = true
        self.addSubview(backgroundView)
        
        // thumb
        self.thumbView = UIView(frame: CGRectMake(1, 1, self.frame.size.height - 2, self.frame.size.height - 2))
        thumbView.backgroundColor = self.thumbTintColor
        thumbView.layer.cornerRadius = (self.frame.size.height * 0.5) - 1
        thumbView.layer.shadowColor = self.shadowColor.CGColor
        thumbView.layer.shadowRadius = 2.0
        thumbView.layer.shadowOpacity = 0.5
        thumbView.layer.shadowOffset = CGSizeMake(0, 3)
        thumbView.layer.shadowPath = UIBezierPath(roundedRect: thumbView.bounds, cornerRadius: thumbView.layer.cornerRadius).CGPath
        thumbView.layer.masksToBounds = false
        thumbView.userInteractionEnabled = false
        self.addSubview(thumbView)
        
        self.switchState = .Off
    }
    
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        super.beginTrackingWithTouch(touch, withEvent: event)
        
        startTrackingValue = self.switchState
        didChangeWhileTracking = false
        
        let activeKnobWidth = self.bounds.size.height - 2 + 5
        isAnimating = true
        
        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut | UIViewAnimationOptions.BeginFromCurrentState, animations: {
            if self.switchState != .Off {
                self.thumbView.frame = CGRectMake(self.bounds.size.width - (activeKnobWidth + 1), self.thumbView.frame.origin.y, activeKnobWidth, self.thumbView.frame.size.height)
                self.backgroundView.backgroundColor = self.onTintColor
                self.thumbView.backgroundColor = self.onThumbTintColor
            }
            else {
                self.thumbView.frame = CGRectMake(self.thumbView.frame.origin.x, self.thumbView.frame.origin.y, activeKnobWidth, self.thumbView.frame.size.height)
                self.backgroundView.backgroundColor = self.activeColor
                self.thumbView.backgroundColor = self.thumbTintColor
            }
            }, completion: { finished in
                self.isAnimating = false
        })
        
        return true
    }
    
    override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        super.continueTrackingWithTouch(touch, withEvent: event)
        
        // Get touch location
        let lastPoint = touch.locationInView(self)
        
        // update the switch to the correct visuals depending on if
        // they moved their touch to the right or left side of the switch
        if lastPoint.x > self.bounds.size.width * 0.5 {
            self.showOn(true)
            if startTrackingValue == .Off {
                didChangeWhileTracking = true
            }
        }
        else {
            self.showOff(true)
            if startTrackingValue == .On {
                didChangeWhileTracking = true
            }
        }
        
        return true
    }
    
    override func endTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) {
        super.endTrackingWithTouch(touch, withEvent: event)
        
        let previousValue = self.switchState
        
        if didChangeWhileTracking {
            self.setState(currentVisualValue, animated: true)
        }
        else {
            switch self.switchState {
            case .Off:
                self.setState(.On, animated: true)
            default:
                self.setState(.Off, animated: true)
            }
        }
        
        if previousValue != self.switchState {
            self.sendActionsForControlEvents(UIControlEvents.ValueChanged)
        }
    }
    
    override func cancelTrackingWithEvent(event: UIEvent?) {
        super.cancelTrackingWithEvent(event)
        
        // just animate back to the original value
        switch self.switchState {
        case .On:
            self.showOn(true)
        case .Off:
            self.showOff(true)
        case .Indeterminate:
            self.showIndeterminate(true)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !isAnimating {
            let frame = self.frame
            
            // background
            backgroundView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height)
            backgroundView.layer.cornerRadius = self.isRounded ? frame.size.height * 0.5 : 2
            
            // thumb
            let normalKnobWidth = frame.size.height - 2
            switch self.switchState {
            case .On:
                thumbView.frame = CGRectMake(frame.size.width - (normalKnobWidth + 1), 1, frame.size.height - 2, normalKnobWidth)
            case .Off:
                thumbView.frame = CGRectMake(1, 1, normalKnobWidth, normalKnobWidth)
            case .Indeterminate:
                thumbView.frame = CGRectMake(frame.size.width - (normalKnobWidth + 1)*3/2, 1, frame.size.height - 2, normalKnobWidth)
            }
            
            thumbView.layer.cornerRadius = self.isRounded ? (frame.size.height * 0.5) - 1 : 2
        }
    }
    
    /*
    *   Set the state of the switch to on or off, optionally animating the transition.
    */
    func setState(state: SwitchState, animated: Bool) {
        switchValue = state
        
        switch switchValue {
        case .On:
            self.showOn(animated)
        case .Off:
            self.showOff(animated)
        case .Indeterminate:
            self.showIndeterminate(animated)
        }
    }
    
    /*
    *   Detects whether the switch is on or off
    *
    *	@return	BOOL YES if switch is on. NO if switch is off
    */
    func hasState() -> SwitchState {
        return self.switchState
    }
    
    /*
    *   update the looks of the switch to be in the on position
    *   optionally make it animated
    */
    func showOn(animated: Bool) {
        let normalKnobWidth = self.bounds.size.height - 2
        let activeKnobWidth = normalKnobWidth + 5
        if animated {
            isAnimating = true
            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut | UIViewAnimationOptions.BeginFromCurrentState, animations: {
                if self.tracking {
                    self.thumbView.frame = CGRectMake(self.bounds.size.width - (activeKnobWidth + 1), self.thumbView.frame.origin.y, activeKnobWidth, self.thumbView.frame.size.height)
                }
                else {
                    self.thumbView.frame = CGRectMake(self.bounds.size.width - (normalKnobWidth + 1), self.thumbView.frame.origin.y, normalKnobWidth, self.thumbView.frame.size.height)
                }
                
                self.backgroundView.backgroundColor = self.onTintColor
                self.backgroundView.layer.borderColor = self.onTintColor.CGColor
                self.thumbView.backgroundColor = self.onThumbTintColor
                }, completion: { finished in
                    self.isAnimating = false
            })
        }
        else {
            if self.tracking {
                thumbView.frame = CGRectMake(self.bounds.size.width - (activeKnobWidth + 1), thumbView.frame.origin.y, activeKnobWidth, thumbView.frame.size.height)
            }
            else {
                thumbView.frame = CGRectMake(self.bounds.size.width - (normalKnobWidth + 1), thumbView.frame.origin.y, normalKnobWidth, thumbView.frame.size.height)
            }
            
            backgroundView.backgroundColor = self.onTintColor
            backgroundView.layer.borderColor = self.onTintColor.CGColor
            thumbView.backgroundColor = self.onThumbTintColor
        }
        currentVisualValue = .On
    }
    
    /*
    *   update the looks of the switch to be in the off position
    *   optionally make it animated
    */
    func showOff(animated: Bool) {
        let normalKnobWidth = self.bounds.size.height - 2
        let activeKnobWidth = normalKnobWidth + 5
        
        if animated {
            isAnimating = true
            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut | UIViewAnimationOptions.BeginFromCurrentState, animations: {
                if self.tracking {
                    self.thumbView.frame = CGRectMake(1, self.thumbView.frame.origin.y, activeKnobWidth, self.thumbView.frame.size.height);
                    self.backgroundView.backgroundColor = self.activeColor
                }
                else {
                    self.thumbView.frame = CGRectMake(1, self.thumbView.frame.origin.y, normalKnobWidth, self.thumbView.frame.size.height);
                    self.backgroundView.backgroundColor = self.inactiveColor
                }
                
                self.backgroundView.layer.borderColor = self.borderColor.CGColor
                self.thumbView.backgroundColor = self.thumbTintColor
                
                }, completion: { finished in
                    self.isAnimating = false
            })
        }
        else {
            if (self.tracking) {
                thumbView.frame = CGRectMake(1, thumbView.frame.origin.y, activeKnobWidth, thumbView.frame.size.height)
                backgroundView.backgroundColor = self.activeColor
            }
            else {
                thumbView.frame = CGRectMake(1, thumbView.frame.origin.y, normalKnobWidth, thumbView.frame.size.height)
                backgroundView.backgroundColor = self.inactiveColor
            }
            backgroundView.layer.borderColor = self.borderColor.CGColor
            thumbView.backgroundColor = self.thumbTintColor
        }
        
        currentVisualValue = .Off
    }
    
    /*
    *   update the looks of the switch to be in the indeterminate position
    *   optionally make it animated
    */
    func showIndeterminate(animated: Bool) {
        let normalKnobWidth = self.bounds.size.height - 2
        let activeKnobWidth = normalKnobWidth + 5
        if animated {
            isAnimating = true
            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut | UIViewAnimationOptions.BeginFromCurrentState, animations: {
                if self.tracking {
                    self.thumbView.frame = CGRectMake(self.bounds.size.width - (activeKnobWidth + 1)*3/2, self.thumbView.frame.origin.y, activeKnobWidth, self.thumbView.frame.size.height)
                }
                else {
                    self.thumbView.frame = CGRectMake(self.bounds.size.width - (normalKnobWidth + 1)*3/2, self.thumbView.frame.origin.y, normalKnobWidth, self.thumbView.frame.size.height)
                }
                
                self.backgroundView.backgroundColor = self.onTintColor
                self.backgroundView.layer.borderColor = self.onTintColor.CGColor
                self.thumbView.backgroundColor = self.onThumbTintColor
                }, completion: { finished in
                    self.isAnimating = false
            })
        }
        else {
            if self.tracking {
                thumbView.frame = CGRectMake(self.bounds.size.width - (activeKnobWidth + 1)*3/2, thumbView.frame.origin.y, activeKnobWidth, thumbView.frame.size.height)
            }
            else {
                thumbView.frame = CGRectMake(self.bounds.size.width - (normalKnobWidth + 1)*3/2, thumbView.frame.origin.y, normalKnobWidth, thumbView.frame.size.height)
            }
            
            backgroundView.backgroundColor = self.onTintColor
            backgroundView.layer.borderColor = self.onTintColor.CGColor
            thumbView.backgroundColor = self.onThumbTintColor
        }
        currentVisualValue = .Indeterminate
    }
}
